<div class="timeOfDay">
     <g:field name="${name}.hour_input" type="number"
              value="${value?.hour?.toString()?.padLeft(2, '0')}" size="2" min="${minHour}" max="${maxHour}"
              class="twoCharacterInput hour-input"
              data-tooltip="${message(code: 'tooltip.patient.questionnaireSchedule.create.hours')}"/>
     :
     <g:field name="${name}.minute" type="number"
              value="${value?.minute?.toString()?.padLeft(2, '0')}" size="2" min="0" max="59"
              class="twoCharacterInput"
              data-tooltip="${message(code: 'tooltip.patient.questionnaireSchedule.create.minutes')}"/>
     <g:if test="${is12HourClock}">
        <g:select name="${name}.ampm"
                  from="${['AM', 'PM']}"
                  value="${amPm}"
                  class="amPm amPm-input"
                  data-tooltip="${message(code: 'tooltip.patient.questionnaireSchedule.create.amPm')}"/>
     </g:if>

     <g:hiddenField name="${name}.hour" value="${originalValue?.hour?.toString()?.padLeft(2, '0')}"/>

    <%
        if(body) {
            out <<  body()
        }
    %>
</div>
<g:javascript>

    if (!(typeof updateTimeOfDayHour === 'function')) {
        var updateTimeOfDayHour = function (hourToSetSelector, hourInputSelector, amPmSelector) {
            var hours = parseInt($(hourInputSelector).val());
            var $toSet = $(hourToSetSelector);

            if (!(hours > 0)) {
                $toSet.val('');
                return $toSet.val();
            }

            var amPm = $(amPmSelector).val();

            $toSet.val(hours);
            if (amPm == 'PM' && hours < 12) {
                $toSet.val(hours + 12);
            }
            if (amPm == 'AM' && hours == 12) {
                $toSet.val(hours - 12);
            }

            return $toSet.val();
        };
    }

    var escape = function (selector) {
        return selector.replace(/(:|\.|\[|\]|,)/g, "\\$1");
    };

    $(function () {
        // timeOfDay sections can be dynamically added, which is why we listen for change events on the parent element.
        $('.timeOfDay').parent().on("change", function (event) {
            var target = event.target.name;

            var idPrefix = target.slice(0, target.lastIndexOf('.'));
            var hourInputSelector = '#' + escape(idPrefix + '.hour_input');
            var amPmSelector = '#' + escape(idPrefix + '.ampm');
            var hourToSetSelector = 'input[name="' + escape(idPrefix + '\.hour') + '"]';

            updateTimeOfDayHour(hourToSetSelector, hourInputSelector, amPmSelector);
        });
    });
</g:javascript>

