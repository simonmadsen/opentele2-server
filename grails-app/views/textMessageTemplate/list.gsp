
<%@ page import="org.opentele.server.model.TextMessageTemplate" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="textMessageTemplate.list.label" /></title>
</head>
<body>
<div id="list-textMessageTemplate" class="content scaffold-list" role="main">
    <h1><g:message code="textMessageTemplate.heading.label"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="name" title="${message(code: 'textMessageTemplate.name.label')}" />
            <th><g:message code="textMessageTemplate.content.label"/></th>

        </tr>
        </thead>
        <tbody>
        <g:each in="${templateList}" status="i" var="template">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show" id="${template.id}">${template.shortName()}</g:link></td>
                <td>${template.shortContent()}</td>

            </tr>
        </g:each>
        </tbody>
    </table>
    <div class="pagination">
        <g:paginate total="${templateListTotal}" />
    </div>

    <fieldset class="buttons">
        <g:link class="create" action="create">
            <g:message code="default.create.label" args="[g.message(code:'textMessageTemplate.label')]" />
        </g:link>
    </fieldset>
</div>
</body>
</html>
