import grails.plugin.springsecurity.SecurityConfigType
import org.apache.log4j.DailyRollingFileAppender

grails.config.locations = [
    "file:${userHome}/.opentele/clinician.properties",
    "file:/etc/opentele/clinician.properties",
    "file:c:/kihdatamon/settings/datamon-config.properties"
]

logging.suffix = "" // used to set a app specific suffix to log files. In case several apps deployed on same tomcat

grails.databinding.convertEmptyStringsToNull = false

grails.session.timeout.default = 30

grails.project.groupId = appName
grails.mime.file.extensions = true
grails.mime.use.accept.header = true
grails.mime.types = [ html: ['text/html','application/xhtml+xml'],
                      xml: ['text/xml', 'application/xml'],
                      text: 'text/plain',
                      js: 'text/javascript',
                      rss: 'application/rss+xml',
                      atom: 'application/atom+xml',
                      css: 'text/css',
                      csv: 'text/csv',
                      all: '*/*',
                      json: ['application/json','text/json'],
                      form: 'application/x-www-form-urlencoded',
                      multipartForm: 'multipart/form-data'
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.includes = ['/font/**', '/images/**', '/css/**', '/js/**', '/plugins/**']

// The default codec used to encode data with ${}
grails.views.default.codec='html'
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"

// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// enable query caching by default
grails.hibernate.cache.queries = false

// Using database-migrations from core plugin
def pluginDir = org.codehaus.groovy.grails.plugins.GrailsPluginUtils.getPluginDirForName('opentele-server-core-plugin')?.path
grails.plugin.databasemigration.changelogLocation = "$pluginDir/grails-app/migrations"


// Som standard er email disabled og lige sÃ¥ greenmail. I de miljÃ¸er hvor det skal enables, skal der stÃ¥
// grails {
//   mail {
//     disabled = false
//   }
//}
//greenmail.disabled=false
// samt den Ã¸vrige mail konfiguration
grails {
    mail {
        disabled = true
        'default' {
            from="svar-ikke@opentele.dk"
        }
    }
}
greenmail.disabled = true
cpr.lookup.enabled = false

milou {
    run = false
    repeatIntervalMillis = 180000

    realtimeRun = false
    realtimeRepeatIntervalMillis = 60000
}

kihdb {
    run = false
    repeatIntervalMillis = 180000
}

video {
    enabled = false
    connection {
        timeoutMillis = 5 * 60 * 1000 // 5 minutes
        asyncTimeoutMillis = 6 * 60 * 1000 // 6 minutes
    }
}

workLog {
    enabled = false
}

medicineList {
    enabled = false
}

sms {
    enabled = false
}

lab {
    enabled = false
    intervalInDays = 90
    timeoutInSeconds = 30
    cacheTimeInMinutes = 30
    nknTableUrl = "http://www.medcom.dk/dwn6460"
    nknTablePropertiesPath = 'nknTable.properties'
    url
    username
    password
    organization
    npuCodes = []
}

help {
    image {
        contentTypes = ["image/jpeg","image/pjpeg","image/png","image/x-png"]
        uploadPath = "helpimg-upload" //In production this path should be configured to a full local path. NOT a path relatvie to web-app!
        providedImagesPath = "helpimg-upload"
        overwriteExistingFiles = true
    }
}

milou.realtimectg.url = ''
milou.realtimeRun = false
milou.realtimeRepeatIntervalMillis = 10000
running.ctg.messaging.enabled = false

defaultLocale = Locale.forLanguageTag("da-DK")

measurement.results.tables.css = 'measurement_results_tables.css'

patient.identification.isCpr = true


environments {

    development {
        grails.logging.jul.usebridge = true

        sms {
            enabled = true
            gateway = 'http://localhost:5678/smsgateway/api/sms/send'
        }

        workLog.enabled = true

        medicineList {
            enabled = true
            convertDocuments = true
        }

        lab {
            enabled = true
            username = ""
            password = ""
            organization = ""
            url = ""
        }


        grails.plugin.databasemigration.dropOnStart = false
        grails.plugin.databasemigration.updateOnStart = false

        kihdb.run = false
        // Systemnavn som overfÃ¸res til KIH Databasen, hvor navnet
        // anvendes til at vise hvor maalinger kommer fra.
        //kihdb.createdByText = "OpenTele udvikling"
        //kihdb.service.url = "https://kihdb-test.rn.dk/services/monitoringDataset"
        grails.plugin.springsecurity.controllerAnnotations.staticRules = [
                '/greenmail/**': ['IS_AUTHENTICATED_ANONYMOUSLY']
        ]
    }

    db_development {

        sms {
            enabled = true
            gateway = 'http://localhost:5678/smsgateway/api/sms/send'
        }
        workLog.enabled = true
        medicineList.enabled = true

        grails.logging.jul.usebridge = true

       grails.plugin.databasemigration.dropOnStart = true
        grails.plugin.databasemigration.updateOnStart = true
        grails.plugin.databasemigration.updateOnStartFileNames = ['changelog.groovy']
        grails.plugin.databasemigration.autoMigrateScripts = ['RunApp', 'TestApp']

        kihdb.run = true
        // Systemnavn som overføres til KIH Databasen, hvor navnet
        // anvendes til at vise hvor maalinger kommer fra.
        kihdb.createdByText = "OpenTele udvikling"
        kihdb.service.url = "https://kihdb-test.rn.dk/services/monitoringDataset"

    }
}

// Logging
String commonPattern = "%d [%t] %-5p %c{2} %x - %m%n"
String logDirectory = "${System.getProperty('catalina.base') ?: '.'}/logs"
def appContext = { logging.suffix != "" ? "-${logging.suffix}" : "" }

log4j = {
    appenders {

        console name: "stdout",
                layout: pattern(conversionPattern: commonPattern)
        appender new DailyRollingFileAppender(
                name:"stacktrace", datePattern: "'.'yyyy-MM-dd",
                file:"${logDirectory}/stacktrace${appContext()}.log",
                layout: pattern(conversionPattern: commonPattern))
        appender new DailyRollingFileAppender(
                name:"opentele", datePattern: "'.'yyyy-MM-dd",
                file:"${logDirectory}/opentele${appContext()}.log",
                layout: pattern(conversionPattern: commonPattern))
    }

    error  'org.codehaus.groovy.grails.web.servlet',  //  controllers
            'org.codehaus.groovy.grails.web.pages', //  GSP
            'org.codehaus.groovy.grails.web.sitemesh', //  layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping', // URL mapping
            'org.codehaus.groovy.grails.commons', // core / classloading
            'org.codehaus.groovy.grails.plugins', // plugins
            'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration,
            'org.codehaus.groovy.grails.commons.cfg',
            'org.springframework',
            'org.hibernate',
            'org.apache',
            'net.sf.ehcache.hibernate',
            'grails.app.services.org.grails.plugin.resource',
            'grails.app.taglib.org.grails.plugin.resource',
            'grails.app.resourceMappers.org.grails.plugin.resource',
            'grails.app.service.grails.buildtestdata.BuildTestDataService',
            'grails.app.buildtestdata',
            'grails.app.services.grails.buildtestdata',
            'grails.buildtestdata.DomainInstanceBuilder'

    root {
        error 'opentele', 'stdout'
    }

    environments {
        development {
            debug 'grails.app',
                    'org.opentele',
                    'grails.app.jobs'
//            debug 'org.hibernate.SQL'
//           trace 'org.hibernate.type'
        }
        db_development {
            debug 'grails.app',
                    'org.opentele',
                    'grails.app.jobs'
//            debug 'org.springframework.security'
//           trace 'org.hibernate.type'
        }
        test {
            debug 'grails.app',
                    'org.opentele'
        }
        datamon_test {
            info    'grails.app',
                    'org.opentele'
        }
        performance {
            debug 'grails.app',
                    'org.opentele',
                    'grails.app.jobs'
        }
    }
}

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'org.opentele.server.model.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'org.opentele.server.model.UserRole'
grails.plugin.springsecurity.authority.className = 'org.opentele.server.model.Role'
grails.plugin.springsecurity.useBasicAuth = true
grails.plugin.springsecurity.basic.realmName = "OpenTele Server"
grails.plugin.springsecurity.useSecurityEventListener = true
grails.plugin.springsecurity.securityConfigType = SecurityConfigType.Annotation
grails.plugin.springsecurity.password.algorithm = 'SHA-256'
grails.plugin.springsecurity.password.hash.iterations = 1

grails.plugin.springsecurity.controllerAnnotations.staticRules = [
        '/dbconsole/**': [org.opentele.server.core.model.types.PermissionName.WEB_LOGIN],
        '/patient/createPatient': [org.opentele.server.core.model.types.PermissionName.PATIENT_CREATE],
        '/dbconsole/**': [org.opentele.server.core.model.types.PermissionName.WEB_LOGIN],
]

grails.plugin.springsecurity.filterChain.chainMap = [
        '/**': 'JOINED_FILTERS,-basicAuthenticationFilter,-basicExceptionTranslationFilter'
]

grails.plugin.springsecurity.providerNames = [
        'caseInsensitivePasswordAuthenticationProvider',
        'anonymousAuthenticationProvider',
        'rememberMeAuthenticationProvider'
]


passwordRetryGracePeriod=120
passwordMaxAttempts=3
reminderEveryMinutes=15