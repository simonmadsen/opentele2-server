package org.opentele.server.provider

import grails.plugin.springsecurity.annotation.Secured
import org.opentele.server.core.model.types.PermissionName
import org.opentele.server.model.TextMessageTemplate

@Secured(PermissionName.NONE)
class TextMessageTemplateController {

    def textMessageTemplateService

    @Secured(PermissionName.TEXT_MESSAGE_TEMPLATE_READ_ALL)
    def index() {
        redirect(action: "list", params: params)
    }

    @Secured(PermissionName.TEXT_MESSAGE_TEMPLATE_READ_ALL)
    def list() {

        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [templateList: TextMessageTemplate.list(params), templateListTotal: TextMessageTemplate.count()]
    }

    @Secured(PermissionName.TEXT_MESSAGE_TEMPLATE_READ)
    def show() {
        def template = TextMessageTemplate.get(params.long("id"))
        if (!template) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'textMessageTemplate.label', default: 'Standard Text Message')])
            redirect(action: "list")
            return
        }

        [template: template]
    }

    @Secured(PermissionName.TEXT_MESSAGE_TEMPLATE_CREATE)
    def create() {
        def template = new TextMessageTemplate()
        [template: template]
    }

    @Secured(PermissionName.TEXT_MESSAGE_TEMPLATE_CREATE)
    def save() {
        def template = textMessageTemplateService.createTemplate(params.name.toString(), params.content.toString())
        if (template.hasErrors()) {
            render(view: "create", model: [template: template])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'textMessageTemplate.label', default: 'Standard Text Message')])
        redirect(action: "list")
    }

    @Secured(PermissionName.TEXT_MESSAGE_TEMPLATE_WRITE)
    def edit() {
        def template = TextMessageTemplate.get(params.id)
        if (!template) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'textMessageTemplate.label', default: 'Standard Text Message')])
            redirect(action: "list")
            return
        }

        [template: template]
    }

    @Secured(PermissionName.TEXT_MESSAGE_TEMPLATE_WRITE)
    def update() {
        try {
            def updated = textMessageTemplateService.updateTemplate(params.long("id"), params.long("version"), params.name.toString(), params.content.toString())
            if (updated.hasErrors()) {
                render(view: "edit", model: [template: updated])
                return
            }

            flash.message = message(code: 'default.updated.message', args: [message(code: 'textMessageTemplate.label', default: 'Standard Text Message')])
            redirect(action: "show", id: updated.id)
        } catch (EntityNotFoundException) {
            flash.message = message(code: 'textMessageTemplate.deleted.by.other')
            redirect(action: "list")
        }
    }

    @Secured(PermissionName.TEXT_MESSAGE_TEMPLATE_DELETE)
    def delete() {
        def messageCode = 'default.deleted.message'
        try {
            textMessageTemplateService.deleteTemplate(params.long("id"))
        } catch (EntityNotFoundException) {
            messageCode = 'default.not.found.message'
        }

        flash.message = message(code: messageCode, args: [message(code: 'textMessageTemplate.label', default: 'Standard Text Message')])
        redirect(action: "list")
    }
}
