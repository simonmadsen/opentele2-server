package org.opentele.server.provider.model

import org.opentele.server.core.test.AbstractIntegrationSpec
import org.opentele.server.model.Department
import org.opentele.server.model.Patient
import org.opentele.server.model.Patient2PatientGroup
import org.opentele.server.model.PatientGroup
import org.opentele.server.model.StandardThresholdSet
import org.opentele.server.core.model.types.PatientState
import org.opentele.server.core.model.types.Sex
import org.opentele.server.provider.PatientGroupService

class PatientGroupControllerIntegrationSpec extends AbstractIntegrationSpec {

    def grailsApplication
    PatientGroupService patientGroupService

    PatientGroupController controller

    def setup() {
        controller = new PatientGroupController()
        controller.patientGroupService = patientGroupService
        authenticate('helleandersen', 'helleandersen1')
    }

    def "should be able to delete patient group without references"() {
        given:
        StandardThresholdSet st = new StandardThresholdSet()
        st.save(failOnError:true)

        Department deptB = Department.findByName("Afdeling-B Test")

        PatientGroup pg = new PatientGroup(name: "RemoveGroup",
                department: deptB, standardThresholdSet: st)
        pg.save(failOnError:true)

        when:
        controller.request.method = 'POST'
        controller.params.id = pg.id
        controller.delete()
        pg = PatientGroup.findById(pg.id)

        then:
        pg == null
    }

    def "should be unable to delete patient group referenced by patient"() {
        given:
        Patient p = new Patient(firstName:'Test',
                lastName:'P.G. Remove',
                cpr:'1231234234',
                sex: Sex.FEMALE,
                address:'Vej1',
                postalCode:'8000',
                city:'Aarhus C',
                state: PatientState.ACTIVE)
        p.save(failOnError: true)

        StandardThresholdSet st = new StandardThresholdSet()
        st.save(failOnError:true)

        Department deptB = Department.findByName("Afdeling-B Test")
        PatientGroup pg = new PatientGroup(name: "RemoveGroup",
                department: deptB, standardThresholdSet: st)
        pg.save(failOnError:true)

        Patient2PatientGroup.link(p, pg)
        p.refresh()

        when:
        controller.request.method = 'POST'
        controller.params.id = pg.id
        controller.delete()
        pg = PatientGroup.findById(pg.id)

        then:
        pg != null
    }

}
